import im from '../assets/img/sprite.png'
import sv from '../assets/svg/big-cheese.svg'
import {Game} from './class'
import {sum} from './sum'

const myGame = new Game()

export function make() {
  const frm = document.getElementById('main')
  const p = `I like ${myGame.name} from JavaScript and sum 2+3=` + sum(2, 3);
  frm.innerHTML = `
<h2>Hello, baby from JavaScript!</h2>
<ul>
  <li>GitLab Pipeline</li>
  <li>Webpack 5</li>
  <li>Jest</li>
</ul>

<hr />
<p>${p} <img src="${im}" alt="img" /></p>

<img src="${sv}" alt="svg" class="img_svg" />`
}
