import '../styles/main.scss'
import '../styles/style.css'

import {make} from './app'

window.document.addEventListener('DOMContentLoaded', make)
